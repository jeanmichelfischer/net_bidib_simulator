# net_bidib_simulator



## Getting started

on raspberrypi with **debian bullseye**

clone this repo

```
git clone https://gitlab.com/jeanmichelfischer/net_bidib_simulator.git
```

install needed packages

```bash
sudo apt install cmake libavahi-common-dev libavahi-client-dev libavahi-core-dev g++
```

build

```bash
cd net_bidib_simulator
cmake .
make
```

and run

```bash
./net_bidib_simulator
```
