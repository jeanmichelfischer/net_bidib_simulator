

#define TCP_PORT 62875

#define PRODUCT_ID 0x0B00

#define AVAHI_ENABLED


#define   BIDIB_VENDOR_ID      0x0D
#define   BIDIB_PRODUCT_ID     0xDDDD     // see www.opendcc.de/elektronik/bidib
#define   CLASS_OCCUPANCY         0     // 0: no occupancy messages, 1: occupancy messages included
#define   CLASS_BOOST             0     // 0: no booster,         1: booster included
#define   CLASS_DCC_MAIN          0     // 0: no command station, 1: command station included
#define   CLASS_SWITCH            1     // 0: no switch messages, 1: switch messages included
#define   CLASS_ACCESSORY         1


