//
// Created by jmf on 21.12.23.
//

#include <thread>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <iostream>
#include <fcntl.h>
#include <cstring>

#include "net_bidib_client.h"
#include "config.h"
#include "bidib_messages.h"

#include "bidib_client_parser.h"

net_bidib_client::net_bidib_client() {
    g_tcp_connect = TCP_DISCONNECTED;

    m_parser = new bidib_client_parser(this);

    std::thread(&net_bidib_client::client_task, this).detach();
}

bool net_bidib_client::connect() {

    int bind_err;

    struct sockaddr_in6 destAddr = {};
    destAddr.sin6_addr;
    destAddr.sin6_family = AF_INET6;
    destAddr.sin6_port = htons(TCP_PORT);


    // Create TCP socket
    bidib_client_if_socket_id = socket(AF_INET6, SOCK_STREAM, IPPROTO_TCP);
    if (bidib_client_if_socket_id < 0) {
        printf("Unable to create socket: errno %d: %s", errno, strerror(errno));
        return false;
    }
    //ESP_LOGI(TAG, "Socket created");


    int opt = 1;
    if (setsockopt(bidib_client_if_socket_id, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(int)) < 0) {
        printf("Setting socket options error: %d: %s", errno, strerror(errno));
        return false;
    }

    // Bind a socket to the specific IP + port
    bind_err = bind(bidib_client_if_socket_id, (struct sockaddr *) &destAddr, sizeof(destAddr));
    if (bind_err != 0) {
        printf("Socket unable to bind: errno %d: %s", errno, strerror(errno));
        return false;
    }

    return true;


    return false;
}

[[noreturn]] void net_bidib_client::client_task() {

    uint8_t rx_buffer[2048];
    ///bzero(rx_buffer, sizeof(rx_buffer));

    int bytes_received;
    int listen_error;

    datawait = 0;

    while (true) { 

        listen_error = listen(bidib_client_if_socket_id, 0);
        if (listen_error != 0) {
            printf("Error occured during listen: errno %d: %s\n", errno, strerror(errno));
            continue;
        }

        printf("socket is listening\n");

        struct sockaddr_in sourceAddr = {}; // Large enough for IPv4
        socklen_t addrLen = sizeof(sourceAddr);
        /* Accept connection to incoming client */
        bidib_client_if_socket = accept(bidib_client_if_socket_id, (struct sockaddr *) &sourceAddr, &addrLen);
        if (bidib_client_if_socket < 0) {
            //ESP_LOGE(TAG, "Unable to accept connection: errno %d: %s", errno, strerror(errno));
            std::this_thread::sleep_for(std::chrono::milliseconds(200));
            continue;
        }
        printf("socket accepted\n");

        g_tcp_connect = TCP_CONNECTED;

        //If O_NONBLOCK is set then recv() will return, otherwise it will stall until data is received or the connection is lost.
        fcntl(bidib_client_if_socket, F_SETFL, O_NONBLOCK);

        while (g_tcp_connect == TCP_CONNECTED) {

            bytes_received = recv(bidib_client_if_socket, &rx_buffer[0], sizeof(rx_buffer), 0);

            if (bytes_received == 0) {
                printf("connection closed\n");
                g_tcp_connect = TCP_DISCONNECTED;
                break;
            } else if (bytes_received > 0) {
                //ESP_LOGI(TAG, "IN");
                // at least ping recieved
                datawait = 0;
                // send it up
                m_parser->pre_process_bidib_message(&rx_buffer[0], bytes_received);
            }

            std::this_thread::sleep_for(std::chrono::milliseconds(10));
            datawait++; // timeout up

            if (datawait > 10000) {
                std::cout << "Connection closed (timeout)\n";
                g_tcp_connect = TCP_DISCONNECTED;
                break;
            }
        }

    }
}

bool net_bidib_client::bidib_tx_fifo_put(unsigned char *new_message) {

    int ex = send(bidib_client_if_socket, new_message, *new_message + 1, 0);
    if( ex <= 0) {
        g_tcp_connect = TCP_DISCONNECTED;
        return 0;
    }

    return ex;
}






