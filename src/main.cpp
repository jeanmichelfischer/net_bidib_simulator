//
// Created by jmf on 21.12.23.
//

#include <iostream>
#include <vector>
#include <thread>

#include "avahi_publisher.h"
#include "net_bidib_client.h"

int main(const int argc, const char **argv) {

    auto client = new net_bidib_client();
    client->connect();


#ifdef AVAHI_ENABLED
    std::vector<std::string> message;
    message.push_back("uid=05000D000BEEEE");
    message.push_back("bidib=node");
    message.push_back("node=true");
    message.push_back("prod=netbidibsim");

    libpublisher::avahi_publisher new_publish("netbidibsim", TCP_PORT, "_bidib._tcp", message);
#elif
    while (true) {
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    }
#endif
}

