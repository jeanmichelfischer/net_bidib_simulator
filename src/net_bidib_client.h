//
// Created by jmf on 21.12.23.
//

#ifndef NET_BIDIB_SIMULATOR_NET_BIDIB_CLIENT_H
#define NET_BIDIB_SIMULATOR_NET_BIDIB_CLIENT_H


#include "bidib_messages.h"
#include "config.h"

class bidib_client_parser;

class net_bidib_client {

public:
    net_bidib_client();


    bool connect();

    bool bidib_tx_fifo_put(unsigned char *new_message);

private:

    [[noreturn]] void client_task();


    typedef enum {
        TCP_DISCONNECTED = 0x00,
        TCP_CONNECTED = 0x01,
    } tcp_connect_t;

    tcp_connect_t g_tcp_connect = TCP_DISCONNECTED;


    int bidib_client_if_socket_id = 0;
    int bidib_client_if_socket = 0;

    int datawait = 0;


    bidib_client_parser* m_parser;
};


#endif //NET_BIDIB_SIMULATOR_NET_BIDIB_CLIENT_H
