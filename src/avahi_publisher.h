#ifndef _PUBLISHER_H
#define _PUBLISHER_H

#include <iostream>
#include <string>
#include <vector>

#include <avahi-client/client.h>
#include <avahi-client/publish.h>

#include <avahi-common/alternative.h>
#include <avahi-common/simple-watch.h>
#include <avahi-common/malloc.h>
#include <avahi-common/error.h>
#include <avahi-common/timeval.h>

namespace libpublisher {
    class avahi_publisher {
    private:
        static void entry_group_callback(AvahiEntryGroup *_group, AvahiEntryGroupState _state, void *userdata);

        static void create_services(AvahiClient *_client);

        static void client_callback(AvahiClient *_client, AvahiClientState _state, void *userdata);

        void treat_collision(AvahiClient *_client);

    public:
        void publish_avahi_service(const char *_service_name, const int _port_number, const char *_service_type);

        avahi_publisher(const std::string &service_name, const int &port_number, const std::string &service_type,
                        std::vector<std::string> &message);

        ~avahi_publisher();
    };
}

#endif //_PUBLISHER_H