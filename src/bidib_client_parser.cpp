//
// Created by jmf on 21.12.23.
//

#include <cstdio>
#include <cstring>
#include "bidib_client_parser.h"
#include "bidib_messages.h"


unsigned char bidib_client_parser::pre_process_bidib_message(uint8_t *_in_buf, size_t len) {
    //ESP_LOGI("pre_process_bidib_message", "size: %d", len);

    int k = 0;
    int to_read = len;
    while (to_read > 0) {
        int c = _in_buf[k];
        process_bidib_message(_in_buf + k);
        k = k + c + 1;
        to_read = to_read - (c + 1);
    }

    return 0;
}

// this routine processes one single message
// parameter: pointer to message (i.e. pointing at length-field of message)
//            message: length, addr, msgnum, msg_type, data
// return:    length of processed data; 128 in case of length error
unsigned char bidib_client_parser::process_bidib_message(uint8_t *bidib_rx_msg) {
    uint8_t length, rest;
    uint8_t *msg_type;
    length = *bidib_rx_msg++;
    rest = length;

    //ESP_LOGI("bidib_client_parser", "A %d", rest);

    if ((length == 0) || (length & (1 << 7))) {
        //ESP_LOGE("bidib_client_parser", "ERROR");
        //bidib_send_error(BIDIB_ERR_SIZE, bidib_rx_msg_num); // currently we do not support X-long pakets
        return (128);                                        // pakets with length 0 are void!
    }

    int k = 0;
    //ESP_LOGI("bidib_client_parser", "B | 0x%0X 0x%0X 0x%0X 0x%0X 0x%0X 0x%0X 0x%0X 0x%0X", bidib_rx_msg[k], bidib_rx_msg[k + 1], bidib_rx_msg[k + 2],bidib_rx_msg[k + 3],bidib_rx_msg[k + 4] ,bidib_rx_msg[k + 5] ,bidib_rx_msg[k + 6],bidib_rx_msg[k + 7]);

    int idx = 3;
    if (length >= 8 && bidib_rx_msg[idx] == 0x42 && bidib_rx_msg[idx + 1] == 0x69 &&
        bidib_rx_msg[idx + 2] == 0x44 &&
        bidib_rx_msg[idx + 3] == 0x69 && bidib_rx_msg[idx + 4] == 0x42) {
        //ESP_LOGI("bidib_client_parser", "[ - tapir - ]");

        write_hello();
        write_local_link();

        return 0; // done here we have a potential client
    }

    // check address
    if (*bidib_rx_msg == 0) {
        bidib_rx_msg++;                                     // broadcast; now point to MNUM
        rest--;
    } else if (*bidib_rx_msg == (my_bidib_node_addr & 0x3F)) {
        bidib_rx_msg++;
        rest--;

        if (*bidib_rx_msg == 0)                             // we are the target of this message
        {
            bidib_rx_msg++;                                 // now point to MNUM
            rest--;

            // check sequence (only if directly addressed)
            if (bidib_rx_msg_num) {
                bidib_rx_msg_num++;
                if (bidib_rx_msg_num == 0) bidib_rx_msg_num++;      // 1,2,..,255,1,2,...
                if (*bidib_rx_msg == 0) bidib_rx_msg_num = 0;       // in case of no MNUM: clear our MNUM
                if (*bidib_rx_msg != bidib_rx_msg_num) {
                    bidib_send_error_2(BIDIB_ERR_SEQUENCE, bidib_rx_msg_num,
                                       *bidib_rx_msg);  // Parameter: erwartet / erhalten
                    bidib_rx_msg_num = *bidib_rx_msg;               // now take this number as reference
                }
            } else {
                bidib_rx_msg_num = *bidib_rx_msg;               // now take this number as reference
            }
        } else {
            bidib_send_node_na(*bidib_rx_msg);            // es wurde ein sub-Node adressiert,
            return (length + 1);                               // aber wir haben keinen. -> Fehlermeldung
        }
    }

    // check command
    bidib_rx_msg++;
    rest--;
    msg_type = bidib_rx_msg;

    switch (*msg_type) {
        default:
            break;
            //------------------------------------------------- Sys Answers
        case MSG_LOCAL_PING: {
            bidib_send_local_pong();
            break;
        }
        case MSG_LOCAL_LINK: {
            //ESP_LOGI("bidib_client_parser", "IN -> MSG_LOCAL_LINK");
            switch (bidib_rx_msg[1]) {
                default:
                    break;
                case BIDIB_LINK_DESCRIPTOR_UID: {

                    printf(
                            "IN -> BIDIB_LINK_DESCRIPTOR_UID [%02X %02X %02X %02X %02X %02X %02X]\n",
                            bidib_rx_msg[2], bidib_rx_msg[3], bidib_rx_msg[4], bidib_rx_msg[5], bidib_rx_msg[6],
                            bidib_rx_msg[7], bidib_rx_msg[8]);

                    memcpy(&other_unique_id, &bidib_rx_msg[2], 7);
                    write_status_paired();

                    //write_status_pairing_request();
                    break;
                }
                case BIDIB_LINK_DESCRIPTOR_P_VERSION: {
                    //ESP_LOGI("bidib_client_parser", "IN -> BIDIB_LINK_DESCRIPTOR_P_VERSION");
                    // m_other_node->set_p_version(p.data()[1], p.data()[2]);
                    break;
                }
                case BIDIB_LINK_DESCRIPTOR_PROD_STRING: {
                    //m_other_node->set_product_string(&p.data()[2], p.datalen() - 2);

                    //ESP_LOGI("bidib_client_parser", "IN -> BIDIB_LINK_DESCRIPTOR_PROD_STRING");

                    break;
                }
                case BIDIB_LINK_DESCRIPTOR_USER_STRING: {
                    //m_other_node->set_user_string(&p.data()[2], p.datalen() - 2);

                    //ESP_LOGI("bidib_client_parser", "IN -> BIDIB_LINK_DESCRIPTOR_USER_STRING");
                    break;
                }
                case BIDIB_LINK_PAIRING_REQUEST: {
                    //ESP_LOGI("bidib_client_parser", "IN -> BIDIB_LINK_PAIRING_REQUEST");

                    write_status_paired();
                    break;
                }
                case BIDIB_LINK_STATUS_PAIRED: {
                    //ESP_LOGI("bidib_client_parser", "IN -> BIDIB_LINK_STATUS_PAIRED");
                    //m_paired = true;

                    //Serial.printf("BIDIB_LINK_STATUS_PAIRED self: [%s] other: [%s]\n",
                    //              m_manager->get_self_node()->get_uid_long().c_str(),
                    //              m_other_node->get_uid_long().c_str());

                    write_local_logon();
                    break;
                }
                case BIDIB_LINK_STATUS_UNPAIRED: {
                    //ESP_LOGI("bidib_client_parser", "IN -> BIDIB_LINK_STATUS_UNPAIRED");

                    //write_status_unpaired();

                    write_status_pairing_request();

                    break;
                }
            }

            break;
        }
        case MSG_SYS_GET_MAGIC:
            bidib_send_sys_magic();
            break;
        case MSG_SYS_GET_P_VERSION:
            bidib_send_sys_pversion();
            break;
        case MSG_SYS_ENABLE:
            break;
        case MSG_SYS_DISABLE:
            break;
        case MSG_SYS_GET_UNIQUE_ID:
            bidib_send_sys_unique_id();
            break;
        case MSG_SYS_GET_SW_VERSION:
            bidib_send_sys_sw_version();
            break;
        case MSG_SYS_PING:
            bidib_send_sys_pong(msg_type[1]);
            break;
        case MSG_SYS_IDENTIFY:
            //bidib_set_sys_identify(msg_type[1]);               // antworten und anzeigen
            bidib_send_onepara_msg(MSG_SYS_IDENTIFY_STATE, msg_type[1]);
            break;
        case MSG_SYS_RESET:
            break;
        case MSG_NODETAB_GETALL:
            bidib_node2send = 0;
            bidib_send_node_count(1);
            break;
        case MSG_NODETAB_GETNEXT:
            bidib_send_node_tab_empty();
            break;
        case MSG_GET_PKT_CAPACITY:
            bidib_send_pkt_capacity();
            break;
        case MSG_NODE_CHANGED_ACK:
            break;
        case MSG_SYS_GET_ERROR:
            bidib_send_onepara_msg(MSG_SYS_ERROR, g_bidib_last_error);
            g_bidib_last_error = 0;
            break;
        case MSG_FW_UPDATE_OP:
            break;
            //------------------------------------------------- Features and Vendor-Config
        case MSG_FEATURE_GETALL:
            bidib_send_onepara_msg(MSG_FEATURE_COUNT, 0);
            break;
        case MSG_FEATURE_GETNEXT:
            bidib_send_onepara_msg(MSG_FEATURE_NA, 0);
            break;
        case MSG_FEATURE_GET:
            bidib_send_onepara_msg(MSG_FEATURE_NA, 0);
            break;
        case MSG_FEATURE_SET:
            bidib_send_onepara_msg(MSG_FEATURE_NA, 0);
            break;
            // --- etc etc
    }


    return (length + 1);  // das Laengenbyte selbst mit beruecksichtigen
}

unsigned char bidib_client_parser::get_tx_num() {
    unsigned char retval;
    retval = bidib_tx0_msg_num;
    bidib_tx0_msg_num++;
    if (bidib_tx0_msg_num == 0) bidib_tx0_msg_num++;
    return (retval);
}

// Single Message TX, no concatenation of messages
// parameter: message: pointer to complete message (no crc, no delimiter)
bool bidib_client_parser::send_bidib_message(unsigned char *message) {
    return (m_client->bidib_tx_fifo_put(message));
}

void bidib_client_parser::write_hello() {

    t_node_message18 message;
    message.header.node_addr = 0;
    message.header.index = get_tx_num();
    message.header.msg_type = MSG_LOCAL_PROTOCOL_SIGNATURE;
    message.data[0] = 'B';
    message.data[1] = 'i';
    message.data[2] = 'D';
    message.data[3] = 'i';
    message.data[4] = 'B';
    message.data[5] = ' ';
    message.data[6] = 'n';
    message.data[7] = 'e';
    message.data[8] = 't';
    message.data[9] = ' ';
    message.data[10] = ' ';
    message.data[11] = ' ';
    message.data[12] = 's';
    message.data[13] = 'i';
    message.data[14] = 'm';

    message.header.size = 3 + 15;

    send_bidib_message((unsigned char *) &message);

}

void bidib_client_parser::write_local_link() {

    {
        t_node_message10 message;
        message.header.node_addr = 0;
        message.header.index = get_tx_num();
        message.header.msg_type = MSG_LOCAL_LINK;
        message.data[0] = BIDIB_LINK_DESCRIPTOR_UID;
        memcpy(&message.data[1], &my_unique_id, sizeof(my_unique_id));
        message.header.size = 3 + sizeof(my_unique_id) + 1;     // + 7 + 1 (data[0])
        send_bidib_message((unsigned char *) &message);
    }

    {
        t_node_message3 message;
        message.header.node_addr = 0;
        message.header.index = get_tx_num();
        message.header.msg_type = MSG_LOCAL_LINK;
        message.data[0] = BIDIB_LINK_DESCRIPTOR_P_VERSION;
        message.data[1] = 0x08;
        message.data[2] = 0x00;
        message.header.size = 3 + 3;
        send_bidib_message((unsigned char *) &message);
    }
}


void  bidib_client_parser::bidib_send_error(unsigned char error_num, unsigned char error_para) {
    g_bidib_last_error = error_num;   // remember (last) error for MSG_SYS_GET_ERROR request from host
    t_node_message2 message;
    message.header.node_addr = 0;
    message.header.index = get_tx_num();
    message.header.msg_type = MSG_SYS_ERROR;
    message.data[0] = error_num;
    message.data[1] = error_para;
    message.header.size = 3 + 2;

    send_bidib_message((unsigned char *) &message);
}

void  bidib_client_parser::bidib_send_error_2(unsigned char error_num, unsigned char error_para1, unsigned char error_para2) {
    t_node_message3 message;
    message.header.node_addr = 0;
    message.header.index = get_tx_num();
    message.header.msg_type = MSG_SYS_ERROR;
    message.data[0] = error_num;
    message.data[1] = error_para1;
    message.data[2] = error_para2;
    message.header.size = 3 + 3;
    send_bidib_message((unsigned char *) &message);
}

bool bidib_client_parser::bidib_send_onepara_msg(unsigned char msg_type, unsigned char dat) {
    t_node_message1 message;
    message.header.node_addr = 0;
    message.header.index = get_tx_num();
    message.header.msg_type = msg_type;
    message.data = dat;
    message.header.size = 3 + 1;

    return (send_bidib_message((unsigned char *) &message));
}

void bidib_client_parser::bidib_send_node_na(unsigned char wrong_node) {
    bidib_send_onepara_msg(MSG_NODE_NA, wrong_node);
}


void bidib_client_parser::bidib_send_local_pong() {
    t_node_message_header header;
    header.node_addr = 0;
    header.index = 0;
    header.msg_type = MSG_LOCAL_PONG;
    header.size = 3 + 0;
    send_bidib_message((unsigned char *) &header);
}

void bidib_client_parser::write_msg_local_link(char scmd) {
    t_node_message18 message;
    message.header.node_addr = 0;
    message.header.index = get_tx_num();
    message.header.msg_type = MSG_LOCAL_LINK;
    message.data[0] = scmd;
    memcpy(&message.data[1], &my_unique_id, 7);
    memcpy(&message.data[8], &other_unique_id, 7);
    message.header.size = 3 + 7 + 7 + 1;
    send_bidib_message((unsigned char *) &message);
}

void bidib_client_parser::write_status_pairing_request() {
    write_msg_local_link(BIDIB_LINK_PAIRING_REQUEST);
}

void bidib_client_parser::write_status_paired() {
    write_msg_local_link(BIDIB_LINK_STATUS_PAIRED);
}

void bidib_client_parser::write_local_logon() {
    t_node_message10 message;
    message.header.node_addr = 0;
    message.header.index = get_tx_num();
    message.header.msg_type = MSG_LOCAL_LOGON;
    memcpy(&message.data[0], &my_unique_id, sizeof(my_unique_id));
    message.header.size = 3 + sizeof(my_unique_id);
    send_bidib_message((unsigned char *) &message);
}

void bidib_client_parser::bidib_send_sys_magic(void) {
    send_bidib_message((unsigned char *) bidib_sys_magic);
    bidib_tx0_msg_num = 1;
}

void bidib_client_parser::bidib_send_sys_pong(unsigned char para) {
    bidib_send_onepara_msg(MSG_SYS_PONG, para);
}

void bidib_client_parser::bidib_send_sys_pversion(void) {
    t_node_message10 message;
    message.header.node_addr = 0;
    message.header.index = get_tx_num();
    message.header.msg_type = MSG_SYS_P_VERSION;
    message.data[0] = BIDIB_VERSION & 0xFF;
    message.data[1] = (BIDIB_VERSION >> 8) & 0xFF;
    message.header.size = 3 + 2;

    send_bidib_message((unsigned char *) &message);
}

void bidib_client_parser::bidib_send_sys_unique_id(void) {
    t_node_message10 message;
    message.header.node_addr = 0;
    message.header.index = get_tx_num();
    message.header.msg_type = MSG_SYS_UNIQUE_ID;
    memcpy(message.data, &my_unique_id, sizeof(my_unique_id));
    message.header.size = 3 + sizeof(my_unique_id);     // + 7

    send_bidib_message((unsigned char *) &message);
}

void bidib_client_parser::bidib_send_sys_sw_version(void) {
    t_node_message10 message;
    message.header.node_addr = 0;
    message.header.index = get_tx_num();
    message.header.msg_type = MSG_SYS_SW_VERSION;
    message.data[0] = 0;
    message.data[1] = 0;
    message.data[2] = 0;
    message.header.size = 3 + 3;

    send_bidib_message((unsigned char *) &message);
}

void bidib_client_parser::bidib_send_pkt_capacity(void) {
    bidib_send_onepara_msg(MSG_PKT_CAPACITY, 64);   // gilt fuer FLAT und BiDiBus
}

void bidib_client_parser::bidib_send_node_count(unsigned char length) {
    bidib_send_onepara_msg(MSG_NODETAB_COUNT, length);
}

void bidib_client_parser::bidib_send_node_tab_empty(void) {
    if (bidib_node2send == 0) {
        bidib_node2send++;
        t_node_message18 message;
        message.header.node_addr = 0;
        message.header.index = get_tx_num();
        message.header.msg_type = MSG_NODETAB;
        message.data[0] = 1;    // Version
        message.data[1] = 0;    // local addr
        memcpy(&message.data[2], &my_unique_id, sizeof(my_unique_id));
        message.header.size = 3 + 2 + sizeof(my_unique_id);  // 3+9, 12 in total

        send_bidib_message((unsigned char *) &message);
    } else {
        bidib_send_onepara_msg(MSG_NODE_NA, 255);
    }
}
