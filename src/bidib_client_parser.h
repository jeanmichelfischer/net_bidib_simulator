//
// Created by jmf on 21.12.23.
//

#ifndef NET_BIDIB_SIMULATOR_BIDIB_CLIENT_PARSER_H
#define NET_BIDIB_SIMULATOR_BIDIB_CLIENT_PARSER_H

#include <stdint.h>
#include <cstddef>
#include "net_bidib_client.h"

//-------------------------------------------------------------------------------
//
// useful typedefs for bidib messages (flat addressing)

typedef struct {
    unsigned char size;
    unsigned char node_addr;
    unsigned char index;
    unsigned char msg_type;
} t_node_message_header;


typedef struct {
    t_node_message_header header;
    unsigned char data;
} t_node_message1;

typedef struct {
    t_node_message_header header;
    unsigned char data[2];              // be sure, not to load more data here!
} t_node_message2;

typedef struct {
    t_node_message_header header;
    unsigned char data[3];              // be sure, not to load more data here!
} t_node_message3;

typedef struct {
    t_node_message_header header;
    unsigned char data[10];
} t_node_message10;

typedef struct {
    t_node_message_header header;
    unsigned char data[18];
} t_node_message18;

typedef struct {
    t_node_message_header header;
    unsigned char data[28];
} t_node_message28;

class bidib_client_parser {

public:
    bidib_client_parser(net_bidib_client *client) : m_client(client) {}


    unsigned char pre_process_bidib_message(uint8_t *_in_buf, size_t len);

private:
    unsigned char process_bidib_message(uint8_t *bidib_rx_msg);


    net_bidib_client *m_client;


    unsigned char bidib_rx_msg_num = 0;      // counter for rx sequence
    unsigned char bidib_tx0_msg_num = 0;         // this is our running message index

    unsigned char g_bidib_last_error;


public:
    t_bidib_unique_id my_unique_id =
            {
                    {
                            {
                                    .class_switch = CLASS_SWITCH,
                                    .class_booster = 0,
                                    .class_accessory = CLASS_ACCESSORY,
                                    .class_dcc_prog = 0,
                                    .class_dcc_main = 0,
                                    .class_ui = 0,
                                    .class_occupancy = CLASS_OCCUPANCY,
                                    .class_bridge = 0,
                            },
                    },
                    .classx_id = 0x00,
                    .dcc_vendor = 0x0D,
                    {
                            {
                                    .product_id = PRODUCT_ID,
                                    .product_serial = 0xFFFF,
                            }
                    }
            };

    t_bidib_unique_id other_unique_id = {};


    unsigned char my_bidib_node_addr;

    unsigned char bidib_node2send;

    unsigned char get_tx_num(void);

    void write_hello();

    void write_local_link();

    bool send_bidib_message(unsigned char *message);

    void bidib_send_error(unsigned char error_num, unsigned char error_para);

    void bidib_send_error_2(unsigned char error_num, unsigned char error_para1, unsigned char error_para2);

    bool bidib_send_onepara_msg(unsigned char msg_type, unsigned char dat);

    void bidib_send_node_na(unsigned char wrong_node);

    void bidib_send_local_pong();

    void write_msg_local_link(char scmd);

    void write_status_pairing_request();

    void write_status_paired();

    void write_local_logon();

    void bidib_send_sys_magic(void);

    void bidib_send_sys_pong(unsigned char para);


    void bidib_send_sys_pversion(void);

    const unsigned char bidib_sys_magic[6] =         // this is our answer for SYS_MAGIC
            {5,              // size
             0x00,           // addr
             0x00,           // msg_num
             MSG_SYS_MAGIC,
             BIDIB_SYS_MAGIC & 0xFF,
             (BIDIB_SYS_MAGIC >> 8) & 0xFF,
            };

    void bidib_send_sys_unique_id(void);

    void bidib_send_sys_sw_version(void);

    void bidib_send_pkt_capacity(void);

    void bidib_send_node_count(unsigned char length);

    void bidib_send_node_tab_empty(void);
};


#endif //NET_BIDIB_SIMULATOR_BIDIB_CLIENT_PARSER_H
